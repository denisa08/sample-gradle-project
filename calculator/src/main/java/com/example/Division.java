package com.example;

public class Division implements Operation{
    @Override
    public double calculate(double operand1, double operand2) {
        if (operand2 == 0) {
            throw new ArithmeticException("Division by zero is not allowed!");
        }
        return operand1 / operand2;
    }
}
