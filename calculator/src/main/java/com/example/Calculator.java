package com.example;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("CALCULATOR");
        System.out.println("Enter an expression 'operand1 operator operand2' or 'exit' to quit:");

        while (true) {
            System.out.print("Expression: ");
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("exit")) {
                System.out.println("Exiting the calculator.");
                break;
            }

            String[] parts = input.split(" ");
            if (parts.length != 3) {
                System.out.println("Invalid expression. Please use the format 'operand1 operator operand2'.");
                continue;
            }

            try {
                double operand1 = Double.parseDouble(parts[0]);
                String operator = parts[1];
                double operand2 = Double.parseDouble(parts[2]);

                Operation operation = getOperation(operator);
                if (operation == null) {
                    System.out.println("Invalid operator. Please use '+', '-', '*', '/', 'max' or 'min'.");
                    continue;
                }

                double result = operation.calculate(operand1, operand2);
                System.out.println("Result: " + result);
            } catch (NumberFormatException e) {
                System.out.println("Invalid operands. Please enter valid numbers.");
            }
        }

        scanner.close();
    }

    private static Operation getOperation(String operator) {
        switch (operator) {
            case "+":
                return new Addition();
            case "-":
                return new Subtraction();
            case "*":
                return new Multiplication();
            case "/":
                return new Division();
            case "max":
                return new Max();
            case "min":
                return new Min();
            default:
                return null;
        }
    }
}
