package com.example;

public class Max implements Operation {
    @Override
    public double calculate(double operand1, double operand2) {
       if (operand1 > operand2)
           return operand1;
       else
           return operand2;
    }
}
