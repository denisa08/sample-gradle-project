package com.example;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

import org.checkerframework.checker.units.qual.A;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TestCalculator {
    @org.junit.jupiter.api.Test
    public void testAddition() {
        Operation a = new Addition();

        double result = a.calculate(5, 3);
        Assertions.assertEquals(8, result, 0.001);

        double result2 = a.calculate(10.5, 4);
        Assertions.assertEquals(14.5, result2, 0.001);
    }

    @Test
    public void testSubtraction() {
        Operation subtraction = new Subtraction();

        double result = subtraction.calculate(18, 7);
        Assertions.assertEquals(11, result, 0.001);

        double result2 = subtraction.calculate(20.7, 5.4);
        Assertions.assertEquals(15.3, result2, 0.001);
    }

    @Test
    public void testMultiplication() {
        Operation multiplication = new Multiplication();

        double result = multiplication.calculate(10, 11);
        Assertions.assertEquals(110, result, 0.001);

        double result2 = multiplication.calculate(5.3, 8.2);
        Assertions.assertEquals(43.46, result2, 0.001);
    }

    @Test
    public void testDivision() {
        Operation division = new Division();

        double result = division.calculate(81, 9);
        Assertions.assertEquals(9, result, 0.001);

        double result2 = division.calculate(10.5, 4);
        Assertions.assertEquals(2.625, result2, 0.001);
    }

    @Test
    public void testMax() {
        Operation max = new Max();

        double result = max.calculate(81, 9);
        Assertions.assertEquals(81, result, 0.001);

        double result2 = max.calculate(10.5, 4);
        Assertions.assertEquals(10.5, result2, 0.001);
    }

    @Test
    public void testMin() {
        Operation min = new Min();

        double result = min.calculate(81, 9);
        Assertions.assertEquals(9, result, 0.001);

        double result2 = min.calculate(10.5, 4);
        Assertions.assertEquals(4, result2, 0.001);
    }

}
