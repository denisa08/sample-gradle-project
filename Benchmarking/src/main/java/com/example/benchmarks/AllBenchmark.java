package com.example.benchmarks;

import com.example.domain.Order;
import com.example.repo.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class AllBenchmark {
    private final static Collection<Order> orders = List.of(new Order(1, 3, 4),
            new Order(2, 3, 4), new Order(3, 7, 7));
    @State(Scope.Benchmark)
    public static class ArrayListState {
        public ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            arrayListBasedRepository.clear();
            arrayListBasedRepository.addAll(orders);
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void arrayListAdd(ArrayListState state){
        state.arrayListBasedRepository.addAll(state.ordersS);
    }

    @Benchmark
    public void arrayListRemove(ArrayListState state) {
        Order order = new Order(1, 3, 4);
        state.arrayListBasedRepository.remove(order);
    }

    @Benchmark
    public void arrayListContains(ArrayListState state) {
        Order order = new Order(2, 3, 4);
        state.arrayListBasedRepository.contains(order);
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        public HashSetBasedRepository<Order> hashSetBasedRepository = new HashSetBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            hashSetBasedRepository.clear();
            hashSetBasedRepository.addAll(orders);
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void hashSetAdd(HashSetState state){
        state.hashSetBasedRepository.addAll(state.ordersS);
    }

    @Benchmark
    public void hashSetRemove(HashSetState state, Blackhole consumer) {
        Order order = new Order(1, 3, 4);
        state.hashSetBasedRepository.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void hashSetContains(HashSetState state, Blackhole consumer) {
        Order order = new Order(2, 3, 4);
        state.hashSetBasedRepository.add(order);
        state.hashSetBasedRepository.contains(order);
        consumer.consume(order);
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        public TreeSetBasedRepository<Order> treeSetBasedRepository = new TreeSetBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            treeSetBasedRepository.clear();
            treeSetBasedRepository.addAll(orders);
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void treeSetAdd(TreeSetState state){
        state.treeSetBasedRepository.addAll(state.ordersS);
    }

    @Benchmark
    public void treeSetRemove(TreeSetState state, Blackhole consumer) {
        Order order = new Order(1, 3, 4);
        state.treeSetBasedRepository.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void treeSetContains(TreeSetState state, Blackhole consumer) {
        Order order = new Order(2, 3, 4);
        state.treeSetBasedRepository.contains(order);
        consumer.consume(order);
    }



    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        public ConcurrentHashMapBasedRepository<Order> concurrentHashMapBasedRepository = new ConcurrentHashMapBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            concurrentHashMapBasedRepository.clear();
            for(Order o: orders){
                concurrentHashMapBasedRepository.add(o);
            }
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void concurrentHashMapAdd(ConcurrentHashMapState state){
        for(Order o: state.ordersS){
            state.concurrentHashMapBasedRepository.add(o);
        }
    }

    @Benchmark
    public void concurrentHashMapRemove(ConcurrentHashMapState state, Blackhole consumer) {
        Order order = new Order(1, 3, 4);
        state.concurrentHashMapBasedRepository.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void concurrentHashMapContains(ConcurrentHashMapState state, Blackhole consumer) {
        Order order = new Order(2, 3, 4);
        state.concurrentHashMapBasedRepository.contains(order);
        consumer.consume(order);
    }


    @State(Scope.Benchmark)
    public static class EclipseCollectionState {
        public EclipseCollectionsBasedRepository<Order> eclipseCollectionsBasedRepository = new EclipseCollectionsBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            eclipseCollectionsBasedRepository.clear();
            eclipseCollectionsBasedRepository.addAll(orders);
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void eclipseCollectionAdd(EclipseCollectionState state){
        state.eclipseCollectionsBasedRepository.addAll(state.ordersS);
    }

    @Benchmark
    public void eclipseCollectionRemove(EclipseCollectionState state, Blackhole consumer) {
        Order order = new Order(1, 3, 4);
        state.eclipseCollectionsBasedRepository.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void eclipseCollectionContains(EclipseCollectionState state, Blackhole consumer) {
        Order order = new Order(2, 3, 4);
        state.eclipseCollectionsBasedRepository.contains(order);
        consumer.consume(order);
    }



    @State(Scope.Benchmark)
    public static class FastutilState {
        public FastutilBasedRepository<Order> fastutilBasedRepository = new FastutilBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            fastutilBasedRepository.clear();
            fastutilBasedRepository.addAll(orders);
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void fastutilAdd(FastutilState state){
        state.fastutilBasedRepository.addAll(state.ordersS);
    }

    @Benchmark
    public void fastutilRemove(FastutilState state, Blackhole consumer) {
        Order order = new Order(1, 3, 4);
        state.fastutilBasedRepository.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void fastutilContains(FastutilState state, Blackhole consumer) {
        Order order = new Order(2, 3, 4);
        state.fastutilBasedRepository.contains(order);
        consumer.consume(order);
    }



    @State(Scope.Benchmark)
    public static class KolobokeState {
        public KolobokeBasedRepository<Order> kolobokeBasedRepository = new KolobokeBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            kolobokeBasedRepository.clear();
            kolobokeBasedRepository.addAll(orders);
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void kolobokeAdd(KolobokeState state){
        state.kolobokeBasedRepository.addAll(state.ordersS);
    }

    @Benchmark
    public void kolobokeRemove(KolobokeState state, Blackhole consumer) {
        Order order = new Order(1, 3, 4);
        state.kolobokeBasedRepository.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void kolobokeContains(KolobokeState state, Blackhole consumer) {
        Order order = new Order(2, 3, 4);
        state.kolobokeBasedRepository.contains(order);
        consumer.consume(order);
    }



    @State(Scope.Benchmark)
    public static class Trove4jState {
        public Trove4jBasedRepository<Order> trove4jBasedRepository = new Trove4jBasedRepository<>();
        public Collection<Order> ordersS;
        @Setup(Level.Invocation)
        public void doSetup() {
            trove4jBasedRepository.clear();
            trove4jBasedRepository.addAll(orders);
            ordersS = Arrays.asList(new Order(4, 10, 4), new Order(5, 10, 13),
                    new Order(6, 17, 7));
        }
    }
    @Benchmark
    public void trove4jAdd(Trove4jState state){
        state.trove4jBasedRepository.addAll(state.ordersS);
    }

    @Benchmark
    public void trove4jRemove(Trove4jState state, Blackhole consumer) {
        Order order = new Order(1, 3, 4);
        state.trove4jBasedRepository.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void trove4jContains(Trove4jState state, Blackhole consumer) {
        Order order = new Order(2, 3, 4);
        state.trove4jBasedRepository.contains(order);
        consumer.consume(order);
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                .include(AllBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(options).run();
    }
}
