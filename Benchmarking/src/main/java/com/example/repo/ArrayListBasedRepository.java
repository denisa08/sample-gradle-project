package com.example.repo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<>();
    }
    @Override
    public void add(T elem) {
        list.add(elem);
    }

    @Override
    public void remove(T elem) {
        list.remove(elem);
    }

    @Override
    public boolean contains(T elem) {
        return list.contains(elem);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void addAll(Collection<T> objects) {
        list.addAll(objects);
    }

}
