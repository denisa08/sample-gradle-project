package com.example.repo;

import java.util.Collection;

public interface InMemoryRepository<T> {
    void add(T elem);
    void remove(T elem);
    boolean contains(T elem);
    void clear();
    void addAll(Collection<T> objects);
}
