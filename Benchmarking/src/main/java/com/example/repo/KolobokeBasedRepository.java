package com.example.repo;

import com.koloboke.collect.set.ObjSet;
import com.koloboke.collect.set.hash.HashObjSets;

import java.util.Collection;

public class KolobokeBasedRepository<T> implements InMemoryRepository<T> {
    private ObjSet<T> set;

    public KolobokeBasedRepository() {
        set = HashObjSets.newMutableSet();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public void addAll(Collection<T> objects) {
        set.addAll(objects);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
