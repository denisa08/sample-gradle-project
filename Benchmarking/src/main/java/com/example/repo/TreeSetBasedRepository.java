package com.example.repo;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T>{
    private Set<T> set;

    public TreeSetBasedRepository() {
        set = new TreeSet<>();
    }
    @Override
    public void add(T elem) {
        set.add(elem);
    }

    @Override
    public void remove(T elem) {
        set.remove(elem);
    }

    @Override
    public boolean contains(T elem) {
        return set.contains(elem);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public void addAll(Collection<T> objects) {
        set.addAll(objects);
    }
}
