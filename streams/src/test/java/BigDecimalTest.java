import com.example.BigDecimalOperations;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BigDecimalTest {
    public List<BigDecimal> decimalList = new ArrayList<>();

    @Test
    void testBigDecimalSum() {
        decimalList.clear();
        for(int i = 0; i < 50; i++) {
            this.decimalList.add(BigDecimal.valueOf(i));
        }
        BigDecimal sum = BigDecimalOperations.sum(decimalList);
        assertEquals(sum, new BigDecimal("1225"));
    }

    @Test
    void testBigDecimalAverage() {
        decimalList.clear();
        for(int i = 0; i < 50; i++) {
            this.decimalList.add(BigDecimal.valueOf(i));
        }
        BigDecimal average = BigDecimalOperations.average(decimalList);
        assertEquals(average, new BigDecimal("24.50"));
    }

    @Test
    void testBigDecimalTop10() {
        decimalList.clear();
        for(int i = 0; i < 50; i++) {
            this.decimalList.add(BigDecimal.valueOf(i));
        }
        List<BigDecimal> top10 = BigDecimalOperations.top10(decimalList);

        decimalList.sort(Collections.reverseOrder());
        List<BigDecimal> expectedTop10 = decimalList.subList(0, 5);

        assertEquals(expectedTop10, top10);
    }
}
