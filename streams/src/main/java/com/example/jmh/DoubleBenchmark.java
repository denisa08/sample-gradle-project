package com.example.jmh;

import com.example.DoubleOperations;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.*;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)

public class DoubleBenchmark {
    private static final int LIST_SIZE = 100_000_000;
    private static final Random random = new Random();

    //random for Double object and primitive
    public List<Double> doubleListRandomObject;
    public double[] doubleArrayRandomPrimitive;


    //ascending order for Double object and primitive
    public List<Double> doubleListAscendingObject;
    public double[] doubleArrayAscendingPrimitive;


    //descending order for Double object and primitive
    public List<Double> doubleListDescendingObject;
    public double[] doubleArrayDescendingPrimitive;

    @Setup(Level.Invocation)
    public void setUp() {
        //Random order
        doubleListRandomObject = generateRandomList();
        doubleArrayRandomPrimitive = doubleListRandomObject.stream().mapToDouble(Double::doubleValue).toArray();

        //Ascending order
        doubleListAscendingObject = generateAscendingList();
        doubleArrayAscendingPrimitive = doubleListAscendingObject.stream().mapToDouble(Double::doubleValue).toArray();

        //Descending order
        doubleListDescendingObject = generateDescendingList();
        doubleArrayDescendingPrimitive = doubleListDescendingObject.stream().mapToDouble(Double::doubleValue).toArray();
    }

    private List<Double> generateRandomList() {
        return random.doubles(LIST_SIZE).boxed().collect(Collectors.toList());
    }


    private List<Double> generateAscendingList() {
        return DoubleStream.iterate(0, i -> i + 1)
                .limit(LIST_SIZE)
                .boxed()
                .collect(Collectors.toList());
    }

    private List<Double> generateDescendingList() {
        return DoubleStream.iterate(LIST_SIZE - 1, i -> i - 1)
                .limit(LIST_SIZE)
                .boxed()
                .collect(Collectors.toList());
    }

    @Benchmark
    public Double sumDoubleObjectRandom() {
        return new DoubleOperations().sumDoubleObject(doubleListRandomObject);
    }

    @Benchmark
    public Double averageDoubleObjectRandom() {
        return new DoubleOperations().averageDoubleObject(doubleListRandomObject);
    }

    @Benchmark
    public List<Double> top10DoubleObjectRandom() {
        return DoubleOperations.top10DoubleObject(doubleListRandomObject);
    }

    @Benchmark
    public double sumDoublePrimitiveRandom() {
        return new DoubleOperations().sumDoublePrimitive(doubleArrayRandomPrimitive);
    }

    @Benchmark
    public double averageDoublePrimitiveRandom() {
        return new DoubleOperations().averageDoublePrimitive(doubleArrayRandomPrimitive);
    }

    @Benchmark
    public double[] top10DoublePrimitiveRandom() {
        return new DoubleOperations().top10DoublePrimitive(doubleArrayRandomPrimitive);
    }

    @Benchmark
    public Double sumDoubleObjectAscending() {
        return new DoubleOperations().sumDoubleObject(doubleListAscendingObject);
    }

    @Benchmark
    public Double averageDoubleObjectAscending() {
        return new DoubleOperations().averageDoubleObject(doubleListAscendingObject);
    }


    @Benchmark
    public List<Double> top10DoubleObjectAscending() {
        return DoubleOperations.top10DoubleObject(doubleListAscendingObject);
    }

    @Benchmark
    public double sumDoublePrimitiveAscending() {
        return new DoubleOperations().sumDoublePrimitive(doubleArrayAscendingPrimitive);
    }

    @Benchmark
    public double averageDoublePrimitiveAscending() {
        return new DoubleOperations().averageDoublePrimitive(doubleArrayAscendingPrimitive);
    }

    @Benchmark
    public double[] top10DoublePrimitiveAscending() {
        return new DoubleOperations().top10DoublePrimitive(doubleArrayAscendingPrimitive);
    }

    @Benchmark
    public Double sumDoubleObjectDescending() {
        return new DoubleOperations().sumDoubleObject(doubleListDescendingObject);
    }

    @Benchmark
    public Double averageDoubleObjectDescending() {
        return new DoubleOperations().averageDoubleObject(doubleListDescendingObject);
    }

    @Benchmark
    public List<Double> top10DoubleObjectDescending() {
        return DoubleOperations.top10DoubleObject(doubleListDescendingObject);
    }

    @Benchmark
    public double sumDoublePrimitiveDescending() {
        return new DoubleOperations().sumDoublePrimitive(doubleArrayDescendingPrimitive);
    }

    @Benchmark
    public double averageDoublePrimitiveDescending() {
        return new DoubleOperations().averageDoublePrimitive(doubleArrayDescendingPrimitive);
    }

    @Benchmark
    public double[] top10DoublePrimitiveDescending() {
        return new DoubleOperations().top10DoublePrimitive(doubleArrayDescendingPrimitive);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(DoubleBenchmark.class.getSimpleName())
                .jvmArgs("-Xmx8G")
                .forks(1)
                .build();
        new Runner(opt).run();
    }
}
