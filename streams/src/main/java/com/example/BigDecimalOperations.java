package com.example;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalOperations {
    public static BigDecimal sum(List<BigDecimal> decimalList) {
        return decimalList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal average(List<BigDecimal> decimalList) {
        return decimalList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add).divide(new BigDecimal(decimalList.size()), 2, BigDecimal.ROUND_HALF_UP);
    }

    public static List<BigDecimal> top10(List<BigDecimal> decimalList) {
        long count = (long) (decimalList.size() * 0.1);
        return decimalList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(count)
                .collect(Collectors.toList());
    }
}
