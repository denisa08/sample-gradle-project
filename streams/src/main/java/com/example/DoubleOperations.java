package com.example;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DoubleOperations {
    public Double sumDoubleObject(List<Double> doublesList) {
        return doublesList.stream().reduce(0D, Double::sum);
    }
    public Double averageDoubleObject(List<Double> doublesList) {
        return doublesList.stream().reduce(0D, Double::sum) / (doublesList.size());
    }
    public static List<Double> top10DoubleObject(List<Double> doublesList) {
        long count = (long) (doublesList.size() * 0.1);
        return doublesList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(count)
                .collect(Collectors.toList());
    }
    public double sumDoublePrimitive(double[] doublesList) {
       return Arrays.stream(doublesList).sum();
    }
    public double averageDoublePrimitive(double[] doublesList) {
        return Arrays.stream(doublesList).average().orElse(0);
    }
    public double[] top10DoublePrimitive(double[] doublesList) {
        int count = (int) (doublesList.length * 0.1);
        return Arrays.stream(doublesList)
                .boxed()
                .sorted((a, b) -> Double.compare(b, a))
                .limit(count)
                .mapToDouble(Double::doubleValue)
                .toArray();
    }
}
